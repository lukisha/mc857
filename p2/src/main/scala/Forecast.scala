import java.text.SimpleDateFormat
import java.time.temporal.ChronoUnit
import java.util.Date

import com.cloudera.sparkts.models.ARIMA
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.{SparkConf, SparkContext}

import scala.concurrent.duration._

/**
  * Created by Diego Rocha (diego.rocha@movile.com)
  */
object Forecast {
  val dateFormat = new SimpleDateFormat("dd/MM/yyyy")
  val firstDay = dateFormat.parse("01/01/2010")

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("TemperatureForecast").setMaster("local")
    val sc = new SparkContext(conf)

    sc.textFile("/home/diegorocha/Documents/MC851/Projeto 2/scala-forecast/target/scala-2.11/classes/measures.csv")
      .map(_.split(";"))
      .filter(_.length == 16)
      .map(row =>
        ((row(0), row(11), row(12)), // Identifier (station id, city, state)
          (dateFormat.parse(row(1)).getTime - firstDay.getTime) / 1.day.toMillis, // Timestamp
          if (row(8).isEmpty) null.asInstanceOf[Double] else row(8).toDouble, // Temperature
          if (row(3).isEmpty) null.asInstanceOf[Double] else row(3).toDouble, // Precipitation
          if (row(9).isEmpty) null.asInstanceOf[Double] else row(9).toDouble, // Humidity
          if (row(13).isEmpty) null.asInstanceOf[Double] else row(13).toDouble // Altitude
        ))
      .groupBy(_._1)
      .map({ data =>
        val temperatureValues = data._2.filter(_._3 != null).map(line => (line._2.toInt, line._3)).toMap.toVector
        val precipitationValues = data._2.filter(_._4 != null).map(line => (line._2.toInt, line._4)).toMap.toVector
        val humidityValues = data._2.filter(_._5 != null).map(line => (line._2.toInt, line._5)).toMap.toVector
        val altitudeValues = data._2.filter(_._6 != null).map(line => (line._2.toInt, line._6)).toMap.toVector

        val temperatureDenseVector = Vectors.sparse(2383, temperatureValues).toDense
        val precipitationDenseVector = Vectors.sparse(2383, precipitationValues).toDense
        val humidityDenseVector = Vectors.sparse(2383, humidityValues).toDense
        val altitudeDenseVector = Vectors.sparse(2383, altitudeValues).toDense

        (data._1, temperatureDenseVector, precipitationDenseVector, humidityDenseVector, altitudeDenseVector)
      })
      .filter(data => data._2.toArray.exists(_ != 0.0) && data._3.toArray.exists(_ != 0.0))
      .map(data =>
        (data._1, // Identifier
          data._2, // Temperature values
          data._3, // Precipitation values
          data._4, // Humidity values
          data._5, // Precipitation values
          ARIMA.fitModel(1, 0, 1, data._2), // Temperature Model
          ARIMA.fitModel(1, 0, 1, data._3) // Precipitation Model
        ))
      .map(data =>
        (data._1, // Identifier
          data._2, // Temperature values
          data._3, // Precipitation values
          data._4, // Humidity values
          data._5, // Precipitation values
          data._6.forecast(data._2, 7).toArray.drop(2383), // Temperature Forecast
          data._7.forecast(data._3, 7).toArray.drop(2383) // Precipitation Forecast
        ))
      .flatMap(
        data =>
          (1 to 7)
            .zip(data._6)
            .zip(data._7)
            .map(row => (row._1._1, row._1._2, row._2))
            .map(forecast =>
              Array(data._1._1, // Station
                data._1._2, // City
                data._1._3, // State
                dateFormat.format(new Date(firstDay.toInstant.plus(2383 + forecast._1, ChronoUnit.DAYS).toEpochMilli)), // Date
                forecast._2, // Temperature Forecast
                forecast._3 // Precipitation Forecast
            ).mkString(";")))
      .saveAsTextFile("results/forecast")
  }
}
