import java.text.SimpleDateFormat

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.stat.Statistics

import scala.concurrent.duration._

/**
  * Created by Diego Rocha (diego.rocha@movile.com)
  */
object Correlation {
  val dateFormat = new SimpleDateFormat("dd/MM/yyyy")
  val firstDay = dateFormat.parse("01/01/2010")

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("TemperatureForecast").setMaster("local")
    val sc = new SparkContext(conf)

    val measures = sc.textFile("/home/diegorocha/Documents/MC851/Projeto 2/scala-forecast/target/scala-2.11/classes/measures.csv")
      .map(_.split(";"))
      .filter(_.length == 16)
      .map(row =>
        ((row(0), row(11), row(12)), // Identifier (station id, city, state)
          (dateFormat.parse(row(1)).getTime - firstDay.getTime) / 1.day.toMillis, // Timestamp
          if (row(8).isEmpty) null.asInstanceOf[Double] else row(8).toDouble, // Temperature
          if (row(3).isEmpty) null.asInstanceOf[Double] else row(3).toDouble, // Precipitation
          if (row(9).isEmpty) null.asInstanceOf[Double] else row(9).toDouble, // Humidity
          if (row(13).isEmpty) null.asInstanceOf[Double] else row(13).toDouble // Altitude
          ))
      .groupBy(_._1)
      .map({ data =>
        val temperatureValues = data._2.filter(_._3 != null).map(line => (line._2.toInt, line._3)).toMap.toVector
        val precipitationValues = data._2.filter(_._4 != null).map(line => (line._2.toInt, line._4)).toMap.toVector
        val humidityValues = data._2.filter(_._5 != null).map(line => (line._2.toInt, line._5)).toMap.toVector
        val altitudeValues = data._2.filter(_._6 != null).map(line => (line._2.toInt, line._6)).toMap.toVector

        val temperatureDenseVector = Vectors.sparse(2383, temperatureValues).toDense
        val precipitationDenseVector = Vectors.sparse(2383, precipitationValues).toDense
        val humidityDenseVector = Vectors.sparse(2383, humidityValues).toDense
        val altitudeDenseVector = Vectors.sparse(2383, altitudeValues).toDense

        (temperatureDenseVector, precipitationDenseVector, humidityDenseVector, altitudeDenseVector)
      })
      .filter(data =>
        data._1.toArray.exists(_ != 0.0) &&
          data._2.toArray.exists(_ != 0.0) &&
          data._3.toArray.exists(_ != 0.0) &&
          data._4.toArray.exists(_ != 0.0))
      .flatMap(data =>
        (0 to 2382)
          .map(i =>
            Vectors.dense(
              data._1(i), // Temperature
              data._2(i), // Precipitation
              data._3(i), // Humidity
              data._4(i))) // Altitude
      )

    val correlMatrix = Statistics.corr(measures, "pearson")

    // Output
    val properties = Array("Temperature", "Precipitation", "Humidity", "Altitude")
    print("%-13s".format(""))
    properties.foreach(p => print(" | %-13s".format(p)))
    println()

    (0 to 3).foreach(i => {
      print("%-13s".format(properties(i)))
      (0 to 3).foreach(j => {
        print(s" | %.11f".format(correlMatrix(i, j)))
      })

      println()
    })
  }
}
