package br.unicamp;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import java.io.IOException;

public class TemperaturesMean {

  public static class LogFilterMapper extends Mapper<Object, Text, Text, Text> {

    private Text keyText = new Text();
    private Text valueText = new Text();

    public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
      String line = value.toString();
      if (line.contains("temperature for the current day is")) {
        valueText.set(value);
        context.write(keyText, valueText);
      }
    }
  }

  public static class NoActionReducer extends Reducer<Text, Text, Text, Text> {

    public void reduce(Text key, Iterable<Text> values, Context context)
            throws IOException, InterruptedException {
      for (Text val : values) {
        context.write(key, val);
      }
    }
  }

  public static class TokenizerMapper extends Mapper<Object, Text, Text, DoubleWritable> {

    private Text keyText = new Text();
    private DoubleWritable valueDouble = new DoubleWritable();
      
    public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
      String[] parts = value.toString().split(" ");
      keyText.set(parts[1].split("-")[1]); // month
      valueDouble.set(Double.valueOf(parts[9])); // temperature
      context.write(keyText, valueDouble);
    }
  }
  
  public static class MeanReducer extends Reducer<Text, DoubleWritable, Text, DoubleWritable> {
    private DoubleWritable result = new DoubleWritable();

    public void reduce(Text key, Iterable<DoubleWritable> values, Context context)
            throws IOException, InterruptedException {
      double sum = 0;
      int count = 0;
      for (DoubleWritable val : values) {
        sum += val.get();
        count++;
      }
      result.set(sum / count);
      context.write(key, result);
    }
  }

  private static void runJob1(Configuration conf, String input, String output) throws Exception {
    Job job = Job.getInstance(conf, "temperatures mean part 1");
    job.setJarByClass(TemperaturesMean.class);
    job.setMapperClass(LogFilterMapper.class);
    job.setCombinerClass(NoActionReducer.class);
    job.setReducerClass(NoActionReducer.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(Text.class);
    FileInputFormat.addInputPath(job, new Path(input));
    FileOutputFormat.setOutputPath(job, new Path(output));
    if (!job.waitForCompletion(true)) {
      throw new IllegalStateException("Fail to wait for completion of part 1");
    }
  }

  private static void runJob2(Configuration conf, String input, String output) throws Exception {
    Job job = Job.getInstance(conf, "temperatures mean part 2");
    job.setJarByClass(TemperaturesMean.class);
    job.setMapperClass(TokenizerMapper.class);
    job.setCombinerClass(MeanReducer.class);
    job.setReducerClass(MeanReducer.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(DoubleWritable.class);
    FileInputFormat.addInputPath(job, new Path(input));
    FileOutputFormat.setOutputPath(job, new Path(output));
    if (!job.waitForCompletion(true)) {
      throw new IllegalStateException("Fail to wait for completion of part 1");
    }
  }

  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();
    String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
    if (otherArgs.length < 3) {
      System.err.println("Usage: temperatures-mean <in> <intermediate> <out>");
      System.exit(2);
    }

    runJob1(conf, otherArgs[0], otherArgs[1]);
    runJob2(conf, otherArgs[1], otherArgs[2]);
    System.exit(0);
  }
}

