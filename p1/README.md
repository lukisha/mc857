# samples

## Run hadoop using docker
```shell
docker run --name hadoop -p 8088:8088 -p 50070:50070 -it sequenceiq/hadoop-docker:2.7.0 /etc/bootstrap.sh -bash
```

# Build project
```shell
mvn clean compile package
```

# Copy project do container
```shell
docker ps # discover the container_id

# copy the jar
docker cp target/mc857*.jar hadoop:/sample.jar

# copy the input files
docker cp data/* hadoop:/
```

## Run samples
Samples:
 - word-count
 - temperatures-mean

```shell
cd /usr/local/hadoop

# clean up
bin/hdfs dfs -rmdir --ignore-fail-on-non-empty output/temperatures_mean

# copy the input file
bin/hdfs dfs -copyFromLocal /1_temperatures_mean.csv input/

# run the job
bin/hadoop jar /sample.jar temperatures-mean input output

# see the result
bin/hdfs dfs -cat output/temperatures_mean/part-r-00000
```

